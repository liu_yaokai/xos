//
// Created by Yaokai Liu on 1/7/23.
//

#include "xtypes.h"
#include "xchar.h"

enum vga_color {
    VGA_COLOR_BLACK = 0,
    VGA_COLOR_BLUE = 1,
    VGA_COLOR_GREEN = 2,
    VGA_COLOR_CYAN = 3,
    VGA_COLOR_RED = 4,
    VGA_COLOR_MAGENTA = 5,
    VGA_COLOR_BROWN = 6,
    VGA_COLOR_LIGHT_GREY = 7,
    VGA_COLOR_DARK_GREY = 8,
    VGA_COLOR_LIGHT_BLUE = 9,
    VGA_COLOR_LIGHT_GREEN = 10,
    VGA_COLOR_LIGHT_CYAN = 11,
    VGA_COLOR_LIGHT_RED = 12,
    VGA_COLOR_LIGHT_MAGENTA = 13,
    VGA_COLOR_LIGHT_BROWN = 14,
    VGA_COLOR_WHITE = 15,
};

static inline xuByte vga_entry_color(enum vga_color fg, enum vga_color bg)
{
    return fg | bg << 4;
}

static inline xuShort vga_entry(xuByte uc, xuByte color)
{
    return (xuShort) uc | (xuShort) color << 8;
}

xSize strlen(const xAscii* str)
{
    xSize len = 0;
    while (str[len])
        len++;
    return len;
}

static const xSize VGA_WIDTH = 80;
static const xSize VGA_HEIGHT = 25;

xSize terminal_row;
xSize terminal_column;
xuByte terminal_color;
xuShort* terminal_buffer;

void terminal_initialize(void)
{
    terminal_row = 0;
    terminal_column = 0;
    terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
    terminal_buffer = (xuShort*) 0xB8000;
    for (xSize y = 0; y < VGA_HEIGHT; y++) {
        for (xSize x = 0; x < VGA_WIDTH; x++) {
            const xSize index = y * VGA_WIDTH + x;
            terminal_buffer[index] = vga_entry(' ', terminal_color);
        }
    }
}

void terminal_setColor(xuByte color)
{
    terminal_color = color;
}

void terminal_putEntryAt(xAscii c, xuByte color, xSize x, xSize y)
{
    const xSize index = y * VGA_WIDTH + x;
    terminal_buffer[index] = vga_entry(c, color);
}

void terminal_putChar(xAscii c)
{
    terminal_putEntryAt(c, terminal_color, terminal_column, terminal_row);
    if (++terminal_column == VGA_WIDTH) {
        terminal_column = 0;
        if (++terminal_row == VGA_HEIGHT)
            terminal_row = 0;
    }
}

void terminal_write(const xAscii* data, xSize size)
{
    for (xSize i = 0; i < size; i++)
        terminal_putChar(data[i]);
}

void terminal_writeString(const xAscii* data)
{
    terminal_write(data, strlen(data));
}

void kernel_0(void)
{
    /* Initialize terminal interface */
    terminal_initialize();

    /* Newline support is left as an exercise. */
    terminal_writeString((xAscii *)"Hello, kernel World!");
}
